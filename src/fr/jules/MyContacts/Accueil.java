package fr.jules.MyContacts;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.Arrays;

public class Accueil extends Activity {
    /**
     * Interaction avec des contacts dans la base de données
     */
    private ContactBD contactBD;


    /**
     * L'adapter pour afficher les contacts
     */
    private ContactItemAdapter adapter;

    /**
     * Liste des contacts couramment affichée
     */
    ArrayList<Contact> contacts;

    /** Liste de tous les contacts de la base
     *  Evite de faire une requête SQL à chaque fois qu'on veut les récupérer
     */
    ArrayList<Contact> allContacts;

    /** L'Adapter pour le Spinner permettant de filtrer les contacts par groupe */
    ArrayAdapter<String> adapterGroupes;

    private ListView listView;

    /** L'index du contact sélectionné lors du démarrage d'autres activités */
    private int indexContactSelectionne;

    /**
     * Bouton pour ajouter un nouveau contact
     */
    private Button nouveauContact;

    /** Le contact sélectionné lors de l'appui long */
    private Contact contactSelectionne = null;

    /** La boite de dialogue à afficher lors d'un appui long sur un contact */
    private AlertDialog.Builder builderDialog;

    final static int RETOUR_NOUVEAU = 1;
    final static int RETOUR_MODIF = 2;
    final static int RETOUR_ANNULE = 3;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // on récupère le spinner pour sélectionner les groupes
        Spinner spinner = (Spinner) findViewById(R.id.choixSpinner);

        // on récupère la liste des groupes dans les ressources strings.xml
        ArrayList<String> groupes = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.groupes)));

        // on ajoute un élément "Tous" au début pour sélectionner tous les éléments
        groupes.add(0, "Tous");

        // et on crée un ArrayAdapter avec ces valeurs
        adapterGroupes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, groupes);

        // layout d'affiche pour le défilement du Spinner
        adapterGroupes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // et on lie l'ArrayAdapter au spinner
        spinner.setAdapter(adapterGroupes);

        // Attribution listener au spinner
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // le choix sélectionne par l'utilsiateur
                String groupeSelectionne = ((TextView)view).getText().toString();
                if(groupeSelectionne == "Tous") {
                    // on a sélectionne "Tous" donc on réinitialise tous les contacts
                    resetAllContacts();

                } else {
                    // on a choisi un groupe pour filtrer
                    filtrerContactsByGroupe(groupeSelectionne);
                }
            }


            /**
             * Méthode appelée quand un élément sélectionné disparait de la liste (mise à jour
             * de l'ArrayAdapter par exemple)
             */
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // ne rien faire
            }
        });



        contactBD = new ContactBD(getApplicationContext());
        contactBD.open(); //ouverture en écriture

        // récupération de tous les contacts de la base
        contacts = new ArrayList<Contact>();
        contacts = contactBD.getContacts();
        allContacts = (ArrayList<Contact>)contacts.clone();

        // Création de l'Adapter pour afficher les contacts
        adapter = new ContactItemAdapter(Accueil.this, contacts);
        listView = (ListView) findViewById(R.id.listeAmis);
        listView.setAdapter(adapter);

        // Listener quand on clique sur un contact
        // on va démarre rune activité avec une interface et ses infos détaillées
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                indexContactSelectionne = position; // on indique quel contact on a sélectionné

                Contact contact = (Contact) adapter.getItemAtPosition(position); // on récupère le contact à afficher
                // et on va créer un Intent pour démarrer la nouvelle activité
                Intent intent = new Intent(Accueil.this, Infos.class);
                // on stocke les infos à afficher dans l'extra
                intent.putExtra("id", contact.getId());
                intent.putExtra("prenom", contact.getPrenom());
                intent.putExtra("nom", contact.getNom());
                intent.putExtra("telFixe", contact.getTelFixe());
                intent.putExtra("telMobile", contact.getTelMobile());
                intent.putExtra("adresse", contact.getAdresse());
                intent.putExtra("dateAnniv", contact.getDateAnniv());
                intent.putExtra("groupe", contact.getGroupe());
                intent.putExtra("img", contact.getPathImage());
                // on démarre l'activité
                startActivityForResult(intent, RETOUR_NOUVEAU);
            }
        });

        nouveauContact = (Button) findViewById(R.id.nouveau);
        nouveauContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Accueil.this, AjoutContact.class);
                startActivityForResult(intent, 0);
            }
        });

        // Gestion de l'appui long sur un contact
        initAlertContact();
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                contactSelectionne = (Contact)adapter.getItem(position); // on met à jour le contact sélectionné
                builderDialog.show(); // affichage dialogue
                return true;
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch(resultCode) {
            case RETOUR_NOUVEAU:
                // ajout nouveau contact
                Contact newContact = new Contact(intent.getStringExtra("nom"),
                        intent.getStringExtra("prenom"),
                        intent.getStringExtra("groupe"),
                        intent.getStringExtra("dateAnniv"),
                        intent.getStringExtra("telFixe"),
                        intent.getStringExtra("telMobile"),
                        intent.getStringExtra("adresse"),
                        intent.getStringExtra("img"));
                contacts.add(newContact);
                allContacts.add(newContact);
                adapter.notifyDataSetChanged();
                break;

            case RETOUR_MODIF:
                // on a modifié un contact
                Contact contactUpdated = new Contact(intent.getIntExtra("id", -1),
                        intent.getStringExtra("nom"),
                        intent.getStringExtra("prenom"),
                        intent.getStringExtra("groupe"),
                        intent.getStringExtra("dateAnniv"),
                        intent.getStringExtra("telFixe"),
                        intent.getStringExtra("telMobile"),
                        intent.getStringExtra("adresse"),
                        intent.getStringExtra("img"));
                contacts.set(indexContactSelectionne, contactUpdated); // mise à jour du contact dans la liste
                // on modifie aussi le contact dans allContacts
                allContacts = contactBD.getContacts();

                adapter.notifyDataSetChanged();
                break;
        }
        indexContactSelectionne = -1; // on indique qu'il n'y a plus de contact sélectionné
    }


    /**
     * Permet de n'afficher dans la ListView que les contacts du groupe spécifié
     *
     * @param groupe
     * Le groupe des contacts que l'on va afficher
     */
    private void filtrerContactsByGroupe(String groupe) {
        // on commence par vider la liste des contacts affichés
        contacts.clear();

        // on ajoute que les contacts du groupe spécifié
        for(Contact contact : this.allContacts)
            if(contact.getGroupe().compareTo(groupe) == 0)
                contacts.add(contact);

        // on avertit du changement de la liste des contacts
        adapter.notifyDataSetChanged();
    }

    /**
     * On réaffiche tous les contacts dans la ListView
     */
    private void resetAllContacts() {
        contacts.clear(); // on vide la liste
        contacts.addAll(allContacts); // on y met tous les contacts
        adapterGroupes.notifyDataSetChanged();
    }


    /**
     * Initialiser l'Alert Dialog lors de l'appui long sur un contact
     */
    private void initAlertContact() {
        builderDialog = new AlertDialog.Builder(Accueil.this);
        builderDialog.setTitle(getResources().getString(R.string.faites_un_choix));

        ArrayAdapter<String> adapterDialog = new ArrayAdapter<String>(Accueil.this,
                android.R.layout.select_dialog_item);
        // ajout des différentes options
        adapterDialog.add(getResources().getString(R.string.appeler_fixe));
        adapterDialog.add(getResources().getString(R.string.appeler_mobile));
        adapterDialog.add(getResources().getString(R.string.envoyer_sms));
        adapterDialog.add(getResources().getString(R.string.supprimer));

        // on ajoute un bouton pour sortir de la boite de dialogue
        builderDialog.setNegativeButton(getResources().getString(R.string.annuler), // nom du bouton
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss(); // disparition de la boite de dialogue
                    }
                });

        builderDialog.setAdapter(adapterDialog, // ajout de l'adapter avec les choix
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = null; // on déclare l'intent pour changer d'activité
                        switch(which) {
                            // on initialise l'intent en fonction du choix
                            case 0:
                                // appel fixe
                                intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contactSelectionne.getTelFixe()));
                                startActivity(intent); // et on démarre l'activité
                                break;
                            case 1:
                                // appel mobile
                                intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + contactSelectionne.getTelMobile()));
                                startActivity(intent); // et on démarre l'activité
                                break;
                            case 2:
                                // SMS
                                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+contactSelectionne.getTelMobile()));
                                startActivity(intent); // et on démarre l'activité
                                break;
                            case 3:
                                // supprimer
                                askSuppressionContact(contactSelectionne.getId()); // on demande à l'utilisateur s'il veut supprimer le contact
                        }
                    }
                });
    }


    private void askSuppressionContact(int contactID) {
        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        // cas où l'utilisateur souhaite supprimer le contact
                        contactBD.deleteContactById(contactSelectionne.getId()); // on supprime le contact de la base de données
                        Toast.makeText(Accueil.this, getResources().getString(R.string.contact_supprime), Toast.LENGTH_SHORT).show(); // message à l'utilisateur
                        // on supprime le contact des listes
                        allContacts.remove(contactSelectionne);
                        contacts.remove(contactSelectionne);
                        adapter.notifyDataSetChanged();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(Accueil.this);
        builder.setMessage(getResources().getString(R.string.demande_suppression));
        builder.setPositiveButton(getResources().getString(R.string.oui), dialogListener);
        builder.setNegativeButton(getResources().getString(R.string.non), dialogListener);
        builder.show(); // finalement, affichage de la boite de dialogue
    }
}
