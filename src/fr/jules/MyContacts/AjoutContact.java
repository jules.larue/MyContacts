package fr.jules.MyContacts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by jules on 15/03/16.
 */
public class AjoutContact extends Activity {
    /** Bouton pour annuler l'ajout du contact */
    private Button annuler;

    /** Bouton pour créer le contact */
    private Button creer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_person);

        // bouton de création du contact
        creer = (Button)findViewById(R.id.valider_edit_contact);
        creer.setText(getResources().getString(R.string.creerLeContact));
        creer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBD contactBD = new ContactBD(getApplicationContext());
                contactBD.open();
                // on récupère tous les attributs du nouveau contact
                String nom = ((EditText)findViewById(R.id.edit_nom)).getText().toString();
                String prenom = ((EditText)findViewById(R.id.edit_prenom)).getText().toString();
                String groupe = ((Spinner)findViewById(R.id.edit_groupe)).getSelectedItem().toString();
                String telFixe = ((EditText)findViewById(R.id.edit_phone_fixe)).getText().toString();
                String telMobile = ((EditText)findViewById(R.id.edit_phone_mobile)).getText().toString();
                String email = ((EditText)findViewById(R.id.edit_email)).getText().toString();
                String dateAnniv = ((EditText)findViewById(R.id.edit_date_anniv)).getText().toString();
                // et on ajoute le contact dans la base de données
                contactBD.insertContact(new Contact(nom, prenom, groupe, dateAnniv, telFixe, telMobile, email, "")); // pour l'instant l'image de la photo de profil ne vaut rien
                contactBD.close();

                Intent intentResult = new Intent();
                intentResult.putExtra("prenom", prenom);
                intentResult.putExtra("nom", nom);
                intentResult.putExtra("telFixe", telFixe);
                intentResult.putExtra("telMobile", telMobile);
                intentResult.putExtra("adresse", email);
                intentResult.putExtra("dateAnniv", dateAnniv);
                intentResult.putExtra("groupe", groupe);
                intentResult.putExtra("img", "");

                // retour à l'accueil
                setResult(Accueil.RETOUR_NOUVEAU, intentResult);
                finish();
            }
        });

        annuler = (Button)findViewById(R.id.annuler_edit_contact);
        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // on revient à l'activité précédente
                setResult(Accueil.RETOUR_ANNULE); // on indique qu'on a annulé la création d'un nouveau contact
                finish();
            }
        });


    }
}
