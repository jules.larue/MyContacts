package fr.jules.MyContacts;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by gfroger on 12/03/16.
 */
public class BaseSQLite extends SQLiteOpenHelper {

    private static final String TABLE_CONTACTS = "table_contacts";
    private static final String COL_ID = "Id";
    private static final String COL_NOM = "Nom";
    private static final String COL_PRENOM = "Prenom";
    private static final String COL_GROUPE = "Groupe";
    private static final String COL_DATEANNIV = "DateAnniv";
    private static final String COL_TELFIXE = "TelFixe";
    private static final String COL_TELMOBILE = "TelMobile";
    private static final String COL_ADRESSE = "Adresse";
    private static final String COL_PATHIMAGE = "PathImage";


    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_CONTACTS + " ("
            + COL_ID        + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_NOM       + " TEXT NOT NULL, "
            + COL_PRENOM    + " TEXT NOT NULL, "
            + COL_GROUPE    + " TEXT, "
            + COL_DATEANNIV + " TEXT, "
            + COL_TELFIXE   + " TEXT, "
            + COL_TELMOBILE + " TEXT, "
            + COL_ADRESSE   + " TEXT, "
            + COL_PATHIMAGE + " TEXT);";

    public BaseSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_CONTACTS + ";");
        onCreate(db);
    }


}