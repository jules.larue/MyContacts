package fr.jules.MyContacts;

/**
 * Created by gfroger on 12/03/16.
 */
public class Contact {

    private int id;
    private String nom;
    private String prenom;
    private String groupe;
    private String dateAnniv;
    private String telFixe;
    private String telMobile;
    private String adresse;
    private String pathImage;


    public Contact() {
    }

    public Contact(String nom, String prenom, String groupe, String dateAnniv, String telFixe, String telMobile, String adresse, String pathImage) {
        this.nom=nom;
        this.prenom=prenom;
        this.groupe=groupe;
        this.dateAnniv=dateAnniv;
        this.telFixe=telFixe;
        this.telMobile=telMobile;
        this.adresse=adresse;
        this.pathImage=pathImage;


    }


    public Contact(int id, String nom, String prenom, String groupe, String dateAnniv, String telFixe, String telMobile, String adresse, String pathImage) {
        this.id = id;
        this.nom=nom;
        this.prenom=prenom;
        this.groupe=groupe;
        this.dateAnniv=dateAnniv;
        this.telFixe=telFixe;
        this.telMobile=telMobile;
        this.adresse=adresse;
        this.pathImage=pathImage;


    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDateAnniv() {
        return dateAnniv;
    }
    public void setDateAnniv(String dateAnniv) {
        this.dateAnniv = dateAnniv;
    }

    public String getGroupe() {
        return groupe;
    }
    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getTelFixe() {
        return telFixe;
    }
    public void setTelFixe(String telFixe) {
        this.telFixe = telFixe;
    }

    public String getTelMobile() {
        return telMobile;
    }
    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }


    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getPathImage() {
        return pathImage;
    }
    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }


    public String toString() {
        String res="";
        res+="Id : " + id + "\n";
        res+="Nom : " + nom + "\n";
        res+="Prenom : " + prenom + "\n";
        res+="Groupe : " + groupe + "\n";
        res+="DateAnniv : " + dateAnniv + "\n";
        res+="TelFixe : " + telFixe + "\n";
        res+="TelMobile : " + telMobile + "\n";
        res+="Adresse : " + adresse + "\n";
        res+="PathImage : " + pathImage + "\n";

        return res;
    }


}
