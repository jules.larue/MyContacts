package fr.jules.MyContacts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by gfroger on 12/03/16.
 */
public class ContactBD {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "contacts.db";


    private static final String TABLE_CONTACTS = "table_contacts";
    private static final String COL_ID = "Id";
    private static final String COL_NOM = "Nom";
    private static final String COL_PRENOM = "Prenom";
    private static final String COL_GROUPE = "Groupe";
    private static final String COL_DATEANNIV = "DateAnniv";
    private static final String COL_TELFIXE = "TelFixe";
    private static final String COL_TELMOBILE = "TelMobile";
    private static final String COL_ADRESSE = "Adresse";
    private static final String COL_PATHIMAGE = "PathImage";

    private static final int NUM_COL_ID = 0;
    private static final int NUM_COL_NOM = 1;
    private static final int NUM_COL_PRENOM = 2;
    private static final int NUM_COL_GROUPE = 3;
    private static final int NUM_COL_DATEANNIV = 4;
    private static final int NUM_COL_TELFIXE = 5;
    private static final int NUM_COL_TELMOBILE = 6;
    private static final int NUM_COL_ADRESSE = 7;
    private static final int NUM_COL_PATHIMAGE = 8;


    private SQLiteDatabase bdd;

    private BaseSQLite baseSQLite;
    Context cont;

    public ContactBD(Context context) {
        cont=context; baseSQLite = new BaseSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open() {
        bdd = baseSQLite.getWritableDatabase();
    }
    public void close() {
        bdd.close();
    }

    public SQLiteDatabase getBDD() {
        return bdd;
    }


    public long insertNewContact() {
        ContentValues values = new ContentValues();

        values.put(COL_NOM, "");
        values.put(COL_PRENOM, "");
        values.put(COL_GROUPE, "");
        values.put(COL_DATEANNIV, "");
        values.put(COL_TELFIXE, "");
        values.put(COL_TELMOBILE, "");
        values.put(COL_ADRESSE, "");
        values.put(COL_PATHIMAGE, "");

        return bdd.insert(TABLE_CONTACTS, null, values);
    }
    public long insertContact(Contact contact) {
        ContentValues values = new ContentValues();

        values.put(COL_NOM, contact.getNom());
        values.put(COL_PRENOM, contact.getPrenom());
        values.put(COL_GROUPE, contact.getGroupe());
        values.put(COL_DATEANNIV, contact.getDateAnniv());
        values.put(COL_TELFIXE, contact.getTelFixe());
        values.put(COL_TELMOBILE, contact.getTelMobile());
        values.put(COL_ADRESSE, contact.getAdresse());
        values.put(COL_PATHIMAGE, contact.getPathImage());

        return bdd.insert(TABLE_CONTACTS, null, values);
    }

    public int updateContact(int id, Contact newContact) {
        ContentValues values = new ContentValues();

        values.put(COL_NOM, newContact.getNom());
        values.put(COL_PRENOM, newContact.getPrenom());
        values.put(COL_GROUPE, newContact.getGroupe());
        values.put(COL_DATEANNIV, newContact.getDateAnniv());
        values.put(COL_TELFIXE, newContact.getTelFixe());
        values.put(COL_TELMOBILE, newContact.getTelMobile());
        values.put(COL_ADRESSE, newContact.getAdresse());
        values.put(COL_PATHIMAGE, newContact.getPathImage());

        return bdd.update(TABLE_CONTACTS, values, COL_ID + " = " + id, null);
    }

    public int deleteContactById(int id) {
        return bdd.delete(TABLE_CONTACTS, COL_ID + " = " + id, null);
    }
    public int deleteContact(Contact contact) {
        return bdd.delete(TABLE_CONTACTS, COL_ID + " = " + contact.getId(), null);
    }


    private Contact cursorToContact(Cursor c) {
        if (c.getCount() == 0)
            return null;

        c.moveToFirst();
        Contact contact = new Contact();

        contact.setId(c.getInt(NUM_COL_ID));
        contact.setNom(c.getString(NUM_COL_NOM));
        contact.setPrenom(c.getString(NUM_COL_PRENOM));
        contact.setDateAnniv(c.getString(NUM_COL_DATEANNIV));
        contact.setGroupe(c.getString(NUM_COL_GROUPE));
        contact.setTelFixe(c.getString(NUM_COL_TELFIXE));
        contact.setTelMobile(c.getString(NUM_COL_TELMOBILE));
        contact.setAdresse(c.getString(NUM_COL_ADRESSE));
        contact.setPathImage(c.getString(NUM_COL_PATHIMAGE));

        c.close();
        return contact;
    }
    private ArrayList<Contact> cursorToContacts(Cursor c) {

        ArrayList<Contact> resContacts = new ArrayList<Contact>();

        if (c.moveToFirst()) {

            while (c.isAfterLast() == false) {
                Contact contact = new Contact();

                contact.setId(c.getInt(NUM_COL_ID));
                contact.setNom(c.getString(NUM_COL_NOM));
                contact.setPrenom(c.getString(NUM_COL_PRENOM));
                contact.setDateAnniv(c.getString(NUM_COL_DATEANNIV));
                contact.setGroupe(c.getString(NUM_COL_GROUPE));
                contact.setTelFixe(c.getString(NUM_COL_TELFIXE));
                contact.setTelMobile(c.getString(NUM_COL_TELMOBILE));
                contact.setAdresse(c.getString(NUM_COL_ADRESSE));
                contact.setPathImage(c.getString(NUM_COL_PATHIMAGE));

                resContacts.add(contact);
                c.moveToNext();
            }
        }
        return resContacts;
    }



    public Contact getContactById(int id) {

        Cursor c = bdd.query(TABLE_CONTACTS, new String[]{
                        COL_ID,
                        COL_NOM,
                        COL_PRENOM,
                        COL_GROUPE,
                        COL_DATEANNIV,
                        COL_TELFIXE,
                        COL_TELMOBILE,
                        COL_ADRESSE,
                        COL_PATHIMAGE},

                COL_ID + "=" + id, null, null, null, null);
        return cursorToContact(c);
    }


    public ArrayList<Contact> getContacts() {
        Cursor c = bdd.rawQuery("select * from "+TABLE_CONTACTS, null);
        return cursorToContacts(c);
    }


    public ArrayList<Contact> getContactsOrderByNomPrenom() {
        Cursor c = bdd.rawQuery("select * from "+TABLE_CONTACTS+"order by"+COL_NOM+", "+COL_PRENOM, null);
        return cursorToContacts(c);
    }


    public ArrayList<Contact> getContactsByGroup(String groupe) {
        Cursor c = bdd.rawQuery("select * from "+TABLE_CONTACTS+" where "+COL_GROUPE+"="+groupe, null);
        return cursorToContacts(c);
    }
}
