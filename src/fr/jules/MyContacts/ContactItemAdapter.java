package fr.jules.MyContacts;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by jules on 14/03/16.
 * Un Adapter pour afficher des contacts dans une ListView
 */
public class ContactItemAdapter extends BaseAdapter {

    /** L'activité d'accueil où sera affichée la ListView */
    private Activity activity;

    /** La liste de contacts */
    private ArrayList<Contact> listeContacts;

    /** L'inflater pour les vues */
    private LayoutInflater inflater;

    public ContactItemAdapter(Activity activity, ArrayList<Contact> contacts) {
        this.activity = activity;
        this.listeContacts = contacts;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    /**
     * @return la taille de la liste des modèles (ici les contacts)
     */
    public int getCount() {
        return listeContacts.size();
    }

    @Override
    /**
     * @return l'item à la position spécifiée
     */
    public Object getItem(int position) {
        return listeContacts.get(position);
    }

    @Override
    /**
     * @return l'identifiant unique de l'identifiant à la position spécifiée. Ici, la position peut servir d'identifiant
     */
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView prenom_nom;
        TextView telephone;
    }

    @Override
    /**
     * @return la vue à afficher pour l'élément à la position spécifiée
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        View resultView = convertView;
        ViewHolder holder;

        if(convertView==null){
            // Aucun élément n'a encore été affiché : on récupère une nouvelle vue
            resultView = inflater.inflate(R.layout.person_list_item, null);

            // Objet ViewHolder Object pour stocker les éléments du layout person_list_item.xml
            holder = new ViewHolder();
            holder.prenom_nom = (TextView) resultView.findViewById(R.id.prenom_nom_item);
            holder.telephone=(TextView)resultView.findViewById(R.id.telephone_item);

            // Set holder avec le LayoutInflater
            resultView.setTag( holder );
        }
        else {
            // élément déjà affiché : on recycle la vue
            holder = (ViewHolder) resultView.getTag();
        }

        if(listeContacts.size()<=0)
        {
            // aucun contact dans la base
            holder.prenom_nom.setText("Aucun contact");

        }
        else
        {
            // on récupère un contact
            Contact contact = null;
            contact = (Contact) listeContacts.get( position ); // on récupère le contact correspondant à l'item à afficher

            holder.prenom_nom.setText(contact.getPrenom()+" "+contact.getNom()); // affichage prénom et nom
            holder.telephone.setText(contact.getTelMobile()); // affichage téléphone mobile

        }
        return resultView; // on retourne la vue à afficher
    }
}
