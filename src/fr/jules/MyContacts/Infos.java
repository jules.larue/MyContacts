package fr.jules.MyContacts;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.Arrays;

/**
 * Created by jules on 13/03/16.
 */
public class Infos extends Activity {
    private Contact contact;

    Button modifier;
    Button annuler;

    TextView nomPrenom;
    TextView fixe;
    TextView mobile;
    TextView email;
    TextView groupe;
    TextView anniversaire;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infos);
        Intent intent = getIntent(); // on récupère l'intent avec es infos à afficher
        contact = new Contact(intent.getIntExtra("id", -1),
                intent.getStringExtra("nom"),
                intent.getStringExtra("prenom"),
                intent.getStringExtra("groupe"),
                intent.getStringExtra("dateAnniv"),
                intent.getStringExtra("telFixe"),
                intent.getStringExtra("telMobile"),
                intent.getStringExtra("adresse"),
                intent.getStringExtra("img"));

        // on récupère les champs de l'activité
        nomPrenom = (TextView) findViewById(R.id.prenom_nom);
        fixe = (TextView) findViewById(R.id.fixe);
        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);
        groupe = (TextView) findViewById(R.id.groupe);
        anniversaire = (TextView) findViewById(R.id.anniversaire);
        Button modifier = (Button) findViewById(R.id.modifier);

        // on met la photo et les bonnes valeurs dans les champs de texte
        //File image = new File(contact.getPathImage()); // on récupère le fichier image
        //Uri uriImage = Uri.fromFile(image); // on récupère un identifiant unique de l'image
        //photo.setImageURI(uriImage); // et on ajoute l'image dans l'ImageView
        nomPrenom.setText(contact.getPrenom() + " " + contact.getNom());
        mobile.setText(getResources().getString(R.string.mobile) + " : " + contact.getTelMobile());
        fixe.setText(getResources().getString(R.string.fixe) + " : " + contact.getTelFixe());
        email.setText(getResources().getString(R.string.email) + " : " + contact.getAdresse());
        groupe.setText(getResources().getString(R.string.groupe) + " : " + contact.getGroupe());
        anniversaire.setText(getResources().getString(R.string.dateAnniversaire) + " : " + contact.getDateAnniv());


        modifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Infos.this, ModificationContact.class);
                // on stocke les infos à afficher dans l'extra
                intent.putExtra("id", contact.getId());
                intent.putExtra("prenom", contact.getPrenom());
                intent.putExtra("nom", contact.getNom());
                intent.putExtra("telFixe", contact.getTelFixe());
                intent.putExtra("telMobile", contact.getTelMobile());
                intent.putExtra("adresse", contact.getAdresse());
                intent.putExtra("dateAnniv", contact.getDateAnniv());
                intent.putExtra("groupe", contact.getGroupe());
                intent.putExtra("img", contact.getPathImage());
                // on démarre l'activité
                startActivityForResult(intent, Accueil.RETOUR_MODIF);
            }
        });

        annuler = (Button) findViewById(R.id.retour_info);
        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Infos.this, Accueil.class);
                // on stocke les infos à afficher dans l'extra
                intent.putExtra("id", contact.getId());
                intent.putExtra("prenom", contact.getPrenom());
                intent.putExtra("nom", contact.getNom());
                intent.putExtra("telFixe", contact.getTelFixe());
                intent.putExtra("telMobile", contact.getTelMobile());
                intent.putExtra("adresse", contact.getAdresse());
                intent.putExtra("dateAnniv", contact.getDateAnniv());
                intent.putExtra("groupe", contact.getGroupe());
                intent.putExtra("img", contact.getPathImage());
                // on revient à l'activité précédente
                setResult(Accueil.RETOUR_MODIF, intent);
                finish();
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch(resultCode) {
            case Accueil.RETOUR_MODIF:
                // on met à jour le contact
                contact = new Contact(intent.getIntExtra("id", -1),
                        intent.getStringExtra("nom"),
                        intent.getStringExtra("prenom"),
                        intent.getStringExtra("groupe"),
                        intent.getStringExtra("dateAnniv"),
                        intent.getStringExtra("telFixe"),
                        intent.getStringExtra("telMobile"),
                        intent.getStringExtra("adresse"),
                        intent.getStringExtra("img"));

                // et mise à jour de l'affichage
                // pour le spinner on va sélectionner l'index de son groupe, pour cela on récupère
                // la ressource array avec les groupes et on cherche l'index du groupe
                nomPrenom.setText(contact.getPrenom() + " " + contact.getNom());
                mobile.setText(getResources().getString(R.string.mobile) + " : " + contact.getTelMobile());
                fixe.setText(getResources().getString(R.string.fixe) + " : " + contact.getTelFixe());
                email.setText(contact.getAdresse());
                groupe.setText(getResources().getString(R.string.groupe) + " : " + contact.getGroupe());
                anniversaire.setText(getResources().getString(R.string.dateAnniversaire) + " : " +contact.getDateAnniv());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.contact_mis_a_jour), Toast.LENGTH_LONG).show();
                break;

            case Accueil.RETOUR_ANNULE:
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.retour), Toast.LENGTH_LONG).show();
                break;
        }
    }
}
