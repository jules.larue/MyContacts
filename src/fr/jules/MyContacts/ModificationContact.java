package fr.jules.MyContacts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.Arrays;

/**
 * Created by jules on 23/03/16.
 */
public class ModificationContact extends Activity {
    /** Bouton pour annuler l'ajout du contact */
    private Button annuler;

    /** Bouton pour créer le contact */
    private Button mettreAJour;

    /** Le contact qu'on modifie */
    private Contact contact;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_person);

        Intent intent = getIntent();

        contact = new Contact(intent.getIntExtra("id", -1),
                intent.getStringExtra("nom"),
                intent.getStringExtra("prenom"),
                intent.getStringExtra("groupe"),
                intent.getStringExtra("dateAnniv"),
                intent.getStringExtra("telFixe"),
                intent.getStringExtra("telMobile"),
                intent.getStringExtra("adresse"),
                intent.getStringExtra("img"));

        // pré-remplissage des champs de texte
        ((EditText)findViewById(R.id.edit_nom)).setText(contact.getNom());
        ((EditText)findViewById(R.id.edit_prenom)).setText(contact.getPrenom());
        // pour le spinner on va sélectionner l'index de son groupe, pour cela on récupère
        // la ressource array avec les groupes et on cherche l'index du groupe
        int indexGroupe = Arrays.asList(getResources().getStringArray(R.array.groupes)).indexOf(contact.getGroupe());
        ((Spinner)findViewById(R.id.edit_groupe)).setSelection(indexGroupe);
        ((EditText)findViewById(R.id.edit_phone_fixe)).setText(contact.getTelFixe());
        ((EditText)findViewById(R.id.edit_phone_mobile)).setText(contact.getTelMobile());
        ((EditText)findViewById(R.id.edit_email)).setText(contact.getAdresse());
        ((EditText)findViewById(R.id.edit_date_anniv)).setText(contact.getDateAnniv());


        // bouton de création du contact
        mettreAJour = (Button)findViewById(R.id.valider_edit_contact);
        mettreAJour.setText(getResources().getString(R.string.mettre_a_jour_contact));
        mettreAJour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactBD contactBD = new ContactBD(getApplicationContext());
                contactBD.open();
                // on récupère tous les attributs du nouveau contact
                String nom = ((EditText)findViewById(R.id.edit_nom)).getText().toString();
                String prenom = ((EditText)findViewById(R.id.edit_prenom)).getText().toString();
                String groupe = ((Spinner)findViewById(R.id.edit_groupe)).getSelectedItem().toString();
                String telFixe = ((EditText)findViewById(R.id.edit_phone_fixe)).getText().toString();
                String telMobile = ((EditText)findViewById(R.id.edit_phone_mobile)).getText().toString();
                String email = ((EditText)findViewById(R.id.edit_email)).getText().toString();
                String dateAnniv = ((EditText)findViewById(R.id.edit_date_anniv)).getText().toString();
                // et on ajoute le contact dans la base de données
                contactBD.updateContact(contact.getId(), new Contact(nom, prenom, groupe, dateAnniv, telFixe, telMobile, email, "")); // pour l'instant l'image de la photo de profil ne vaut rien
                contactBD.close();

                Intent intentResult = new Intent();
                intentResult.putExtra("prenom", prenom);
                intentResult.putExtra("nom", nom);
                intentResult.putExtra("telFixe", telFixe);
                intentResult.putExtra("telMobile", telMobile);
                intentResult.putExtra("adresse", email);
                intentResult.putExtra("dateAnniv", dateAnniv);
                intentResult.putExtra("groupe", groupe);
                intentResult.putExtra("img", "");

                // retour à l'accueil
                setResult(Accueil.RETOUR_MODIF, intentResult);
                finish();
            }
        });

        annuler = (Button)findViewById(R.id.annuler_edit_contact);
        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // on revient à l'activité précédente
                setResult(Accueil.RETOUR_ANNULE); // on indique qu'on a
                finish();
            }
        });


    }
}
